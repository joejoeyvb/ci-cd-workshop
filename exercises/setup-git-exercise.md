# Setup GIT repository

In this exercise you'll setup your personal copy (fork) on your newly created Gitlab account. 

This will enable you to work on your own deployment pipeline tied to the newly created account using Gitlab CI, without impacting others' work.

## Create an account

This step can be skipped if you already have a GitLab account. Take these steps if you don't have an account yet:

- Go to the [GitLab sign-up page](https://gitlab.com/users/sign_in) and select register. 

![Signup](images/signup.png)

- Fill in the form with correct credentials and a confirmation e-mail will be sent.
- Confirm the account by clicking the confirm link in the e-mail.
- Now sign in with the newly created account on the [GitLab sign-up page](https://gitlab.com/users/sign_in) and login!

## Fork the project

Forking is a means of creating a copy of a project where a project can be extended and reshaped without touching the original project. Take these steps to fork the project:

1. Open Project Overview (top left corner) in a separate tab.
2. Select the fork button (see below) and select your newly created account. 

![Fork](images/fork.png)

3. The project will be forked and will be available within one minute.
4. Afterwards, go to Settings in the lower left corner and select "General" from the resulting menu. 
5. Go to "Visibility, project features, permissions" and set `Project Visibility` to "Private".

You're now ready to work on your own pipeline! Continue to exercise 2: [Setup CI/CD environment](exercises/setup-cicd.md) 
